-- Php Finder --
----------------

-- Description --
-----------------
This module is used to find the php filter format used in node,block and views.
Use to find the particular nodes/views/blocks which uses php filter format.
The developer can reduce the time to find out the nodes/blocks/views.
And can make the changes to the corresponding nodes/blocks/views accordingly.
This module will be listed in the Reports section of the main admin section.

-- Dependencies --
------------------
Drupal 7 latest version.

-- INSTALLATION --
------------------
1. Download and install the Php finder module as normal.

2. Install modules on "sites/all/modules/contrib/" folder.
