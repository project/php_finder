<?php

/**
 * @file
 * php_finder.pages.inc
 */

/**
 * Page callback for find php in node.
 */
function php_finder_node_report() {

  $rows = array();
  // Get field for 'Long text with summary'.
  $db_or = db_or();
  $get_fields = db_select('field_config_instance', 'fci');
  $get_fields->fields('fci', array('field_name'));
  $get_fields->leftJoin('field_config', 'fc', 'fc.field_name = fci.field_name');
  $db_or->condition('fc.type', 'text_with_summary', '=');
  $db_or->condition('fc.type', 'text_long', '=');
  $get_fields->condition($db_or);
  $get_fields->groupBy('fci.field_name');
  $result_get_fields = $get_fields->execute()->fetchAll();

  $nid_set = array();
  if (count($result_get_fields) > 0) {
    foreach ($result_get_fields as $result_field) {
      $table_name = 'field_data_' . $result_field->field_name;
      $field_format_name = $result_field->field_name . '_format';
      $get_node_ids = db_select($table_name, 'n');
      $get_node_ids->fields('n', array('entity_id'));
      $get_node_ids->condition($field_format_name, 'php_code');
      $result_get_node_ids = $get_node_ids->execute()->fetchCol(0);
      if (count($result_get_node_ids) > 0) {
        $nid_set[] = $result_get_node_ids;
      }
    }
  }

  // Merge all inner array values to the same array.
  if ($nid_set) {
    $result_mine = call_user_func_array('array_merge', $nid_set);
  }
  else {
    $result_mine = array();
  }

  if (count($result_mine) > 0) {
    // Removes duplicate node ids.
    $unique_nids = array_unique($result_mine);
    $get_node = db_select('node', 'nd');
    $get_node->fields('nd', array('nid', 'title', 'type', 'status', 'changed'));
    $get_node->condition('nd.nid', $unique_nids, 'IN');
    $result = $get_node->execute()->fetchAll();

    if (count($result) > 0) {
      foreach ($result as $result_node) {
        $op_label = t('edit');
        $op_link = 'node/' . $result_node->nid . '/edit';
        $op_path = l($op_label, $op_link, array('html' => TRUE));
        $rows[] = array(
          'data' => array(
            check_plain($result_node->title),
            $result_node->type,
            $result_node->status ? t('published') : t('not published'),
            format_date($result_node->changed, 'short'),
            $op_path,
          ),
          'class' => array('draggable'),
        );
      }
    }

    $header = array(
      'title' => strtoupper(t('title')),
      'type' => strtoupper(t('type')),
      'status' => strtoupper(t('status')),
      'changed' => strtoupper(t('updated')),
      'operation' => strtoupper(t('operation')),
    );

    $build['node_result_table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    );

    return $build;
  }
  else {
    return t('No content available');
  }
}

/**
 * Page callback for for find php in block.
 */
function php_finder_block_report() {

  $rows = array();
  // Get blocks for using php_code format.
  $php_in_block = db_select('block_custom', 'bc');
  $php_in_block->fields('bc', array('bid', 'info'));
  $php_in_block->condition('bc.format', 'php_code');
  $result = $php_in_block->execute()->fetchAll();
  if (count($result) > 0) {
    foreach ($result as $result_block) {
      $op_label = t('configure');
      $op_link = 'admin/structure/block/manage/block/' . $result_block->bid . '/configure';
      $op_path = l($op_label, $op_link, array('html' => TRUE));
      $rows[] = array(
        'data' => array(
          check_plain($result_block->info),
          $op_path,
        ),
        'class' => array('draggable'),
      );
    }
    $header = array(strtoupper(t('block name')), strtoupper(t('operation')));

    $build['block_result_table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    );

    return $build;
  }
  else {
    return t('No block available');
  }
}

/**
 * Page callback for for find php in view.
 */
function php_finder_view_report() {

  $rows = array();
  // Check views enabled.
  if (module_exists('views')) {
    // Get views for using php_code format.
    $php_in_view = db_select('views_display', 'vd');
    $php_in_view->fields('vv', array('vid', 'name', 'description'));
    $php_in_view->leftJoin('views_view', 'vv', 'vv.vid = vd.vid');
    $php_in_view->condition('vd.display_options', '%php%', 'LIKE');
    $result = $php_in_view->execute()->fetchAll();
    if (count($result) > 0) {
      foreach ($result as $result_view) {
        $op_label = t('edit');
        $op_link = 'admin/structure/views/view/' . check_plain($result_view->name) . '/edit';
        $op_path = l($op_label, $op_link, array('html' => TRUE));
        $rows[] = array(
          'data' => array(
            check_plain($result_view->name),
            check_plain($result_view->description),
            $op_path,
          ),
          'class' => array('draggable'),
        );
      }
      $header = array(
        'view name' => strtoupper(t('view name')),
        'description' => strtoupper(t('description')),
        'operation' => strtoupper(t('operation')),
      );

      $build['views_result_table'] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
      );

      return $build;
    }
    else {
      return t('No views available');
    }

  }
  else {
    return t('Views not enabled');
  }
}
